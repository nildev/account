// +build integration

package account

import (
	"fmt"
	"os"
	"testing"

	. "bitbucket.org/nildev/account/Godeps/_workspace/src/gopkg.in/check.v1"
	"bitbucket.org/nildev/account/oauth"
	"bitbucket.org/nildev/account/provision"
)

type AccountIntegrationSuite struct{}

var _ = Suite(&AccountIntegrationSuite{})

func TestMain(m *testing.M) {
	provision.NildevInitMongoDB()
	code := m.Run()
	//	provision.DestroyMongoDB()
	os.Exit(code)
}

func (s *AccountIntegrationSuite) TestIfAccountWhichDoesNotExistsIsBeingRegistered(c *C) {
	code := "xxxxx"
	rez, err := Register(oauth.PROVIDER_BITBUCKET, code, Data{"scopes": "webhooks, repo"})

	fmt.Printf("%s", err)
	c.Assert(rez, Equals, true)
	c.Assert(err, IsNil)
}

//
//func (s *AccountIntegrationSuite) TestIfCorrectErrorIsReturnedWhenBadProviderIsPassed(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfCorrectErrorIsReturnedWhenBadEmailIsPassed(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfEmailWhichAlreadyExistsIsNotCreatedAgain(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfUsernameWhichAlreadyExistsIsNotCreatedAgain(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfAuthTokenWhichAlreadyExistsIsNotCreatedAgain(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfTryingCreateAccountWhichAlreadyExistsReturnsCorrectError(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfUserIsAbleToLoginByEmail(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfUserIsAbleToLoginByUsername(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfCredentialsAreWrongWhenTryingToAuthByEmailCorrectErrorIsReturned(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfCredentialsAreWrongWhenTryingToAuthByUsernameCorrectErrorIsReturned(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfTokenIsWrongWhenTryingToAuthByUsernameCorrectErrorIsReturned(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfTokenIsWrongWhenTryingToAuthByEmailCorrectErrorIsReturned(c *C) {
//	c.Assert(true, Equals, false)
//}
//
//func (s *AccountIntegrationSuite) TestIfCreatedAccountIsActive(c *C) {
//	c.Assert(true, Equals, false)
//}
