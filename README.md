# Development

## Running integration tests

Create docker machine:
```
docker-machine create --driver virtualbox nildev
```

Setup docker environment:
```
docker-machine start nildev
eval $(docker-machine env nildev)
DM_NILDEV_IP=$(docker-machine ip nildev)
```

Run docker containers:
```
docker run --name local-mongo-nildev -p 27017:27017 -d mongo:3.0.8
```

Execute tests:
```
DM_NILDEV_IP=$(docker-machine ip nildev) \
ND_MONGODB_URL="mongodb://$DM_NILDEV_IP:27017/nildev" \
ND_DATABASE_NAME="nildev" \
ND_BITBUCKET_CLIENT_ID="xxxxxx" \
ND_BITBUCKET_SECRETE="xxxxxxx" \
go test -v -tags integration
```

# Provisioning

Having done it over the code users wil be able to provision with some initial data. Through nildev.io dashboard
they can set ENV variables which will be used to connect to remote source. Also data files can be pushed together with code in repo.

I can use this code to provision it locally as well. It will work same where everywhere.

# Override `registry`

Pass env variables to override what would be returned once deployed in nildev.io

## Testing locally

Get auth code:
```
https://bitbucket.org/site/oauth2/authorize?client_id={client_id}&response_type=code
```

Add to `register.json`:
```
cd $GOPATH/src/bitbucket.org/nildev/builder
accountBuilder
dm ip nildev
docker run -d -p "8080:8080" -e "ND_BITBUCKET_CLIENT_ID=xxx" -e "ND_BITBUCKET_SECRETE=xxxx" -e "ND_DATABASE_NAME=nildev" -e "ND_MONGODB_URL=mongodb://192.168.99.100:27017/nildev" blackhole/account:latest
curl -X POST --data "@register.json" http://192.168.99.100:8080/api/v1/Register -v
```