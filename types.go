package account

import (
	"time"

	"bitbucket.org/nildev/account/Godeps/_workspace/src/gopkg.in/mgo.v2/bson"
)

type (
	// Account type
	Account struct {
		Id    bson.ObjectId `bson:"_id"`
		Token Token         `bson:"token"`
		Data  Data          `bson:"data"`
	}

	// Data used to store arbitrary data for account
	Data map[string]interface{}

	// Token type
	Token struct {
		// AccessToken is the token that authorizes and authenticates
		// the requests.
		AccessToken string `bson:"authToken"`

		// TokenType is the type of token.
		// The Type method returns either this or "Bearer", the default.
		TokenType string `bson:"tokenType,omitempty"`

		// RefreshToken is a token that's used by the application
		// (as opposed to the user) to refresh the access token
		// if it expires.
		RefreshToken string `bson:"refreshToken,omitempty"`

		// Expiry is the optional expiration time of the access token.
		//
		// If zero, TokenSource implementations will reuse the same
		// token forever and RefreshToken or equivalent
		// mechanisms for that TokenSource will not be used.
		Expiry time.Time `bson:"expiry,omitempty"`
	}
)
