package oauth

import (
	"bitbucket.org/nildev/account/Godeps/_workspace/src/github.com/juju/errors"
	"bitbucket.org/nildev/account/Godeps/_workspace/src/golang.org/x/oauth2"
	"bitbucket.org/nildev/account/Godeps/_workspace/src/golang.org/x/oauth2/bitbucket"
	"bitbucket.org/nildev/account/Godeps/_workspace/src/golang.org/x/oauth2/github"
)

const (
	PROVIDER_GITHUB    = "github"
	PROVIDER_BITBUCKET = "bitbucket"
)

func GetEndpoint(provider string) (*oauth2.Endpoint, error) {
	if provider == PROVIDER_BITBUCKET {
		return &bitbucket.Endpoint, nil
	}

	if provider == PROVIDER_GITHUB {
		return &github.Endpoint, nil
	}

	return nil, errors.Trace(errors.Errorf("[%s] provider is not supported", provider))
}
