package account // import "bitbucket.org/nildev/account"

import (
	"os"

	"bitbucket.org/nildev/account/Godeps/_workspace/src/github.com/nildev/lib/registry"
	"bitbucket.org/nildev/account/Godeps/_workspace/src/golang.org/x/oauth2"
	"bitbucket.org/nildev/account/Godeps/_workspace/src/gopkg.in/mgo.v2/bson"
	"bitbucket.org/nildev/account/oauth"
	"bitbucket.org/nildev/account/provision"
)

func Register(provider string, authCode string, data Data) (result bool, err error) {

	clientID := os.Getenv("ND_BITBUCKET_CLIENT_ID")
	clientSecrete := os.Getenv("ND_BITBUCKET_SECRETE")

	endpoint, err := oauth.GetEndpoint(provider)

	if err != nil {
		return false, err
	}

	conf := &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecrete,
		Scopes:       []string{},
		Endpoint:     *endpoint,
	}

	token, err := conf.Exchange(oauth2.NoContext, authCode)

	if err != nil {
		return false, err
	}

	acc := Account{
		Id:   bson.NewObjectId(),
		Data: data,
		Token: Token{
			AccessToken:  token.AccessToken,
			RefreshToken: token.RefreshToken,
			TokenType:    token.TokenType,
			Expiry:       token.Expiry,
		},
	}

	session, err := registry.GetMongoDBClient()
	if err != nil {
		return false, err
	}

	collection := session.DB(registry.GetDatabaseName()).C(provision.TABLE_ACCOUNTS)
	err = collection.Insert(acc)

	if err != nil {
		return false, err
	}

	return true, nil
}

func AuthByEmail(email string, authCode string) (result bool, err error) {
	return true, nil
}

func AuthByUsername(username string, authCode string) (result bool, err error) {
	return true, nil
}
